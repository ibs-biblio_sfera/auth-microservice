from main_app.models import User
from datetime import datetime, timedelta
import jwt
from django.conf import settings

def generate_access_token(user: User) -> str:
    """Метод для генерации токена доступа"""
    payload = {
        "user_id": user.pk,
        "exp": datetime.now() + timedelta(minutes=360),
        "iat": datetime.now(),
    }
    return jwt.encode(payload, settings.SECRET_KEY)


def generate_refresh_token(user: User) -> str:
    """Метод для генерации токена обновления"""
    payload = {
        "user_id": user.pk,
        "exp": datetime.now() + timedelta(days=7),
        "iat": datetime.now(),
    }
    return jwt.encode(payload, settings.SECRET_KEY)
