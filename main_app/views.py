import jwt
from django.conf import settings
from django.contrib.auth import authenticate
from rest_framework import status
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from main_app.tools.jwt_tools import generate_access_token, generate_refresh_token

from .models import User
from .serializers import LoginSerializer, RefreshTokenSerializer, UserSerializer


class RegisterView(APIView):
    """Представление для регистрации нового пользователя"""

    serializer_class = UserSerializer

    def post(self, request: Request) -> Response:
        """Метод для регистрации пользователя"""
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(
                {"message": "User registered successfully"},
                status=status.HTTP_201_CREATED,
            )
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LoginView(APIView):
    """Представление для авторизации пользователя"""

    serializer_class = LoginSerializer

    def post(self, request: Request) -> Response:
        """Метод для авторизации пользователя"""
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            username = serializer.validated_data["username"]
            password = serializer.validated_data["password"]
            user = authenticate(username=username, password=password)
            if user:
                access_token = generate_access_token(user)
                refresh_token = generate_refresh_token(user)
                return Response(
                    {"access_token": access_token, "refresh_token": refresh_token},
                    status=status.HTTP_200_OK,
                )
            return Response(
                {"error": "Invalid credentials"},
                status=status.HTTP_401_UNAUTHORIZED,
            )
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class RefreshTokenView(APIView):
    """Представление для обновления токена доступа"""

    serializer_class = RefreshTokenSerializer

    def post(self, request: Request) -> Response:
        """Метод для обновления токена доступа"""
        refresh_token = self.serializer_class(data=request.data)
        if refresh_token:
            try:
                payload = jwt.decode(
                    refresh_token, settings.SECRET_KEY, algorithms=["HS256"]
                )
                user_id = payload["user_id"]
                user = User.objects.get(pk=user_id)
                access_token = generate_access_token(user)
                return Response(
                    {"access_token": access_token}, status=status.HTTP_200_OK
                )
            except jwt.ExpiredSignatureError:
                return Response(
                    {"error": "Refresh token has expired"},
                    status=status.HTTP_401_UNAUTHORIZED,
                )
            except jwt.InvalidTokenError:
                return Response(
                    {"error": "Invalid refresh token"},
                    status=status.HTTP_401_UNAUTHORIZED,
                )
        else:
            return Response(
                {"error": "Refresh token is required"},
                status=status.HTTP_400_BAD_REQUEST,
            )
