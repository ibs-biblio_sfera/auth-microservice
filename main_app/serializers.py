from django.contrib.auth import authenticate
from rest_framework import serializers

from .models import User


class UserSerializer(serializers.ModelSerializer):
    """Сериализатор для создания пользователя"""

    class Meta:
        model = User
        fields = ["username", "password"]
        extra_kwargs = {"password": {"write_only": True}}

    def create(self, validated_data: dict[str, any]) -> User:
        """Метод создает нового пользователя"""
        user: User = User.objects.create_user(**validated_data)
        return user


class LoginSerializer(serializers.Serializer):
    """Сериализатор для авторизации пользователя"""

    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, data: dict[str, any]) -> dict[str, any]:
        """Метод проверяет валидность данных пользователя"""
        username = data.get("username")
        password = data.get("password")

        if username and password:
            user = authenticate(username=username, password=password)

            if user:
                data["user"] = user
            else:
                raise serializers.ValidationError("Неверные учетные данные")
        else:
            raise serializers.ValidationError(
                "Необходимо ввести имя пользователя и пароль"
            )

        return data

class RefreshTokenSerializer(serializers.Serializer):
    """Сериализатор для обновления токена доступа"""

    refresh_token = serializers.CharField()
